import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import './App.css';
import Layout from './components/Layout';
import Login from './components/Login'
import Pagina404 from './components/Pagina404'

function App() {
  return (
    <BrowserRouter>
      <Layout>
        <Switch>
          <Route exact path="/" component={Login} ></Route>
          <Route path="/404" component={Pagina404} />
          <Redirect from="*" to="/404" />
        </Switch>
      </Layout>
    </BrowserRouter>
  );
}

export default App;
