import React from 'react'
import api from '../api'
import usernameIcon from '../assets/nombre.png'
import passwordKey from '../assets/passwordKey.png'
import '../styles/Login.css'
import '../styles/Information.css'
import Cookies from 'universal-cookie'


class Login extends React.Component {
    cookies = new Cookies();
    
    state = {
        credentials: {
            username: '',
            password: '',
        },
        jwt: null,
        error: null,
        status: null,
    }

    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount(){
        if(this.cookies.get('jwt')){
            this.isTokenExpired();
        }
    }

    isTokenExpired = async () =>{
        
        const isExpired = await api.calls.validate({
            jwt: this.cookies.get('jwt')
        });
        if(!isExpired){
            this.getUser();            
        }
        else{
            this.cookies.set('jwt', '', {path:'/'})
        }
    }

    getJwt = async () => {
        const jwt = await api.calls.autenthicate(this.state.credentials);
        this.setState({
            jwt: jwt.jwt,
            error: jwt.error,
        })
        if(this.state.jwt){
            this.cookies.set('jwt', this.state.jwt, {path:"/"});
            this.getUser();
        }
    }
    
    getUser= async () =>{
        const response = await api.calls.read(1, this.cookies.get('jwt'));
        //console.log(response.error);
        this.setState({response: response})
        this.setState({
            status: true
        })

    }


    handleInput = (e) =>{
        this.setState({
            credentials: {
                ...this.state.credentials,
                [e.target.name] : e.target.value
            }
        })
    }

    handleClick(){
        this.getJwt();
    }

    handleClickLogOut = (e) =>{
        this.setState ({
            credentials: {
                username: '',
                password: '',
            },
            jwt: null,
            error: null,
            status: null,
        })
        this.setState({status:false});
        this.cookies.set('jwt', '', {path:'/'});
    }


    render(){
        if(!this.state.status){
            return <React.Fragment>
                <div className="form__loginTitle form__loginTitle--red" >Login</div>
                <div className="form__textLogin">Por favor ingrese sus credenciales para logearse, recuerde que estos fueron enviados a su correo </div>
                <form className="form">
                        <div className="form__inputs">
                            {this.state.error === "Forbidden" &&(
                                <div class="form__textLogin--Incorrect">El nombre de usuario o la contraseña son incorrectos</div>
                            )}
                            <img className="inputs__icon" src={usernameIcon} alt="Login username"></img>
                            <input onChange={this.handleInput} className="inputs" type="text" name="username" placeholder="Username or ID Scotia" value={this.state.credentials.username} required></input>
                        </div>

                        <div className="form__inputs">
                            <img className="inputs__icon" src={passwordKey} alt="Password Key"></img>
                            <input onChange={this.handleInput} className="inputs" type="password" name="password" placeholder="Password" value={this.state.credentials.password} required></input>
                        </div>
                        <button onClick={this.handleClick} className="form__button" type="button">
                            <div className="button__text">INGRESAR</div>
                        </button>
                </form>
            </React.Fragment>
        }
        else{
            return <React.Fragment>
                <div className="form__loginTitle">Bienvenido {this.state.response.first_name}</div>
                <div style={{padding:"15px"}}>Tus datos son:</div>
                <div className="container__info">
                    <div className="info__data c-one r-one"><b>Nombre:</b> <br></br> {this.state.response.first_name}</div>
                    <div className="info__data c-two r-one"><b>Apellido:</b> <br></br> {this.state.response.last_name}</div>
                </div>
                <div className="container__info">
                    <div className="info__data c-one r-one"><b>Email:</b> <br></br> {this.state.response.email}</div>
                </div>
                <div className="container__info">
                    <div className="info__data c-one r-one"><b>Genero:</b> <br></br> {this.state.response.gender}</div>
                    <div className="info__data c-two r-one"><b>Id Scotia:</b> <br></br> {this.state.response.id}</div>
                </div>
                <div className="container__info">
                    <div className="info__data c-one r-one"><b>Dirección Ip:</b> <br></br> {this.state.response.ip_address}</div>
                </div>

                <button onClick={this.handleClickLogOut} className="form__button" type="button">
                    <div className="button__text">Log Out</div>
                </button>
            </React.Fragment>
        }
    }
}

export default Login;