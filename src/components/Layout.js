import React from 'react';
import heroImage from '../assets/Banco-Scotiabank.jpg'
import scotiaLogo from '../assets/Logo_Scotiabank.png'

function Layout(props){
    return <React.Fragment>
        <div className="container"> 
                <img className="heroImage" src={heroImage} alt="Scotia Bank"></img>
                <div className="container__form">
                    <img className="form__scotiaLogo" src={scotiaLogo} alt="Logo Scotia"></img>
                    {props.children}
                </div>
        </div>
    </React.Fragment>
}

export default Layout;