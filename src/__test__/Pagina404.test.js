import { render, screen } from '@testing-library/react';
import Pagina404 from '../components/Pagina404';

test('Looking for messagge 404', () => {
  render(<Pagina404 />);
  const messageElement = screen.getByText(/Error 404/i);
  expect(messageElement).toBeInTheDocument();
});