const BASE_URL = 'https://apiscotia.herokuapp.com/apiScotia';

async function callApi(endpoint, jwt, options = {}) {
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');

    if(jwt){
        headers.append('Authorization', 'Bearer '+ jwt);
    }
    options.headers = headers;
    const url = BASE_URL + endpoint;
    const response = await fetch(url, options);
    const data = await response.json();

    return data;
}

const api = {
  calls: {
    autenthicate(credentials) {
      return callApi('/auth/authenticate', undefined,{
        method: 'POST',
        body: JSON.stringify(credentials),
      });
    },
    validate(jwt) {
      return callApi('/auth/validate', undefined,{
        method: 'POST',
        body: JSON.stringify(jwt),
      });
    },
    read(userId, jwt) {
      return callApi(`/Users/accessWithId/${userId}`, jwt, {
        method: 'GET',
        mode: 'cors',
      });
    },
  },
};

export default api;